import 'package:flutter/material.dart';
import './product_manager.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Hello Wordl',
      home: Scaffold(
        appBar: AppBar(
          title: Text('FCKAPW'),
        ),
        body: ProductManager('Food Tester'),
      ),
    );
  }
}
