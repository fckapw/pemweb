import 'package:flutter/material.dart';

class Products extends StatelessWidget {
  final List<String> products;
  Products(this.products) {
    print('[Product Widget]) Constructor');
  }
  
  @override
  Widget build(BuildContext context) {
  
    print('[Product Widget] build()');
    return Column(
      children: products
          .map(
            (el) => Card(
                  child: Column(
                    children: <Widget>[
                      Image.asset('assets/win.jpeg'),
                      
                      Text('Pantai Indrayati Gunung Kidul Jogja'),
                    ],
                  ),
                ),
          )
          .toList(),
    );
  }
}
